#!/usr/bin/python

import curses
import time
import bouncing
import move_with_arrows

def main(stdscr):
    curses.start_color()
    stdscr.nodelay(1)
    curses.mousemask(1)
    curses.curs_set(0)

    # todo: color pair management... to avoid overwriting this
    curses.init_pair(1, curses.COLOR_WHITE, curses.COLOR_BLACK)
    stdscr.bkgd(" ", curses.color_pair(1))

    last_frame_time = time.time()
    start_time = time.time()
    desired_fps = 60

    running = True
    i = 1
    m = None
    fps = desired_fps
    render_objects = []
    render_objects.append(bouncing.bouncing(stdscr))
    render_objects.append(bouncing.bouncing(stdscr, 10, 10, True, False))
    render_objects.append(move_with_arrows.move_with_arrows(stdscr))
    while(running):
        stdscr.clear() # erase() wouldn't clear the actual screen
        stdscr.addstr(2, 3, "fps is " + str(fps))
        stdscr.addstr(3, 3, "i is " + str(i))
        stdscr.addstr(5, 3, "stdscr top left: " + str(stdscr.getbegyx()))
        stdscr.addstr(6, 3, "stdscr bottom right: " + str(stdscr.getmaxyx()))
        stdscr.addstr(7, 3, "mouse: " + str(m))
        char = stdscr.getch()
        if char == ord('q'):
            running = False
        if char == curses.KEY_MOUSE:
            m = curses.getmouse()
        for item in render_objects:
            item.key_pressed(char)
            item.update()
        stdscr.refresh()
        i = i + 1
        next_frame_time = start_time + (i * (1.0/desired_fps)) #last_frame_time + (1.0/desired_fps)
        if next_frame_time > last_frame_time:
            time.sleep(next_frame_time - last_frame_time)
        # what about frame skipping?
        last_frame_time = time.time()
        fps = i / (last_frame_time - start_time) # todo: make fps only average last N frames

curses.wrapper(main)
