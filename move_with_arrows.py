import curses

class move_with_arrows:
    def __init__(self, stdscr):
        self.posX = 0
        self.posY = 0
        self.stdscr = stdscr

    def update(self):
        curses.init_pair(2, curses.COLOR_GREEN, curses.COLOR_BLACK)
        self.stdscr.addch(self.posX, self.posY, "@", curses.color_pair(2))

    def key_pressed(self, key):
        if key == curses.KEY_RIGHT:
            self.check_bounds_and_move_to(self.posX, self.posY + 1)
        if key == curses.KEY_LEFT:
            self.check_bounds_and_move_to(self.posX, self.posY - 1)
        if key == curses.KEY_UP:
            self.check_bounds_and_move_to(self.posX - 1, self.posY)
        if key == curses.KEY_DOWN:
            self.check_bounds_and_move_to(self.posX + 1, self.posY)

    def check_bounds_and_move_to(self, x, y):
        (window_min_x, window_min_y) = self.stdscr.getbegyx()
        (window_max_x, window_max_y) = self.stdscr.getmaxyx()
        window_max_x -= 1
        window_max_y -= 1

        if x < window_min_x:
            return
        if x > window_max_x:
            return

        if y < window_min_y:
            return
        if y > window_max_y:
            return

        # For some reason, we can't render to the very bottom right
        if x == window_max_x and y == window_max_y:
            return

        self.posX = x
        self.posY = y
