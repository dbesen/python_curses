import curses

class bouncing:
    def __init__(self, stdscr, posX=0, posY=0, moving_right=True, moving_down=True):
        self.posX = posX
        self.posY = posY
        self.moving_right = moving_right # if false, moving left
        self.moving_down = moving_down # if false, moving up
        self.velocity = 0.2
        self.stdscr = stdscr

    def key_pressed(self, key):
        pass

    def update(self):
        (window_min_x, window_min_y) = self.stdscr.getbegyx()
        (window_max_x, window_max_y) = self.stdscr.getmaxyx()

        curses.init_pair(3, curses.COLOR_BLUE, curses.COLOR_BLACK)
        self.stdscr.addch(self.posX, self.posY, "O", curses.color_pair(3))

        if self.moving_right:
            self.posX = self.posX + self.velocity
        else:
            self.posX = self.posX - self.velocity

        if self.posX <= window_min_x + 1:
            self.moving_right = True
        if self.posX >= window_max_x - 1:
            self.moving_right = False

        if self.moving_down:
            self.posY = self.posY + self.velocity
        else:
            self.posY = self.posY - self.velocity

        if self.posY <= window_min_y + 1:
            self.moving_down = True
        if self.posY >= window_max_y - 1:
            self.moving_down = False
